#include "check.h"
#include <string>
#include "GLES2/gl2.h"
#include <cassert>

void check(const std::string& tag) {
	GLenum err = glGetError();
	if (err != 0) {
		printf("gl error: %s %d\n", tag.c_str(), err);
	}
	
	assert(err == 0);

}
