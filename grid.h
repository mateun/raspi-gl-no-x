#pragma once

#include "GLES2/gl2.h"
#include "shader_util.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

using namespace glm;


class Grid {
	
public: 
	Grid();

	void render(float x, float y, float z, float angle);

private:
	void initShaders();
private:

	GLuint posBuf;
	GLuint shaderProg;
	GLuint attrPos;
	GLuint matWorldIndex=0;
	GLuint matViewIndex=0;
	GLuint matProjIndex=0;
	CUBE_STATE_T* state;
	bool verbose;

};


#ifdef GRID_IMPL

Grid::Grid()  {
	verbose = true;
	initShaders();
	

}
void Grid::render(float x, float y, float z,  float angle) {
	glBindBuffer(GL_ARRAY_BUFFER, posBuf);
  glUseProgram(shaderProg);	

	glm::mat4 mt = glm::translate(glm::mat4(1), glm::vec3(x, y, z));
	glm::mat4 ms = glm::scale(glm::mat4(1), vec3(1, 1, 1));
	glm::mat4 mv = glm::lookAt(vec3{0, 2, 5}, vec3{0, 0, -2}, vec3{0, 1, 0});
	glm::mat4 mp = glm::perspective<float>(radians(30.0f), (float)16./9., 0.01, 100);
	glm::mat4 mw = ms;
	glUniformMatrix4fv(matWorldIndex, 1, false, glm::value_ptr(mw));
	glUniformMatrix4fv(matViewIndex, 1, false, glm::value_ptr(mv));
	glUniformMatrix4fv(matProjIndex, 1, false, glm::value_ptr(mp));
	glDrawArrays(GL_LINES, 0, 2);
	check("after draw lines");
}

void Grid::initShaders() {

	static const GLfloat vertex_data[] = {
		0,0,10,
		0,0,-10,

	};

	const GLchar *vshader_source =
		"attribute vec3 vertex;"
		"uniform mat4 mworld;"
		"uniform mat4 mview;"
		"uniform mat4 mproj;"
		"void main(void) {"

		" vec4 pos = mproj * mview * mworld * vec4(vertex, 1);"
		" gl_Position = pos;"

		"}";

	const GLchar *fshader_source =
		"void main(void) {"
		"  gl_FragColor = vec4(1, 0, 0, 1);"

		"}";


	GLuint vshader = glCreateShader(GL_VERTEX_SHADER);
	printf("grid vshader created\n");
	glShaderSource(vshader, 1, &vshader_source, 0);
	glCompileShader(vshader);
	check();

	if (verbose)
			showlog(vshader);

	GLuint fshader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fshader, 1, &fshader_source, 0);
	glCompileShader(fshader);
	check();

	if (verbose)
			showlog(fshader);


	shaderProg = glCreateProgram();
	glAttachShader(shaderProg, vshader);
	glAttachShader(shaderProg, fshader);
	glLinkProgram(shaderProg);
	check();

	if (verbose)
	  showprogramlog(shaderProg);

  glUseProgram(shaderProg);
						
	matWorldIndex = glGetUniformLocation(shaderProg, "mworld");
	matViewIndex = glGetUniformLocation(shaderProg, "mview");
	matProjIndex = glGetUniformLocation(shaderProg, "mproj");
  attrPos = glGetAttribLocation(shaderProg, "vertex");
	check();
	
	glGenBuffers(1, &posBuf);
	glBindBuffer(GL_ARRAY_BUFFER, posBuf);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_data),
											 vertex_data, GL_STATIC_DRAW);
	glVertexAttribPointer(attrPos, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(attrPos);
	check();
}

#endif

