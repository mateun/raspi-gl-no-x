#pragma once

#include "GLES2/gl2.h"
#include <stdio.h>

void showlog(GLint shader);
void showprogramlog(GLint shader);

#ifdef SHADERUTIL_IMPL
void showlog(GLint shader)
{
   // Prints the compile log for a shader
   char log[1024];
   glGetShaderInfoLog(shader,sizeof log,nullptr,log);
   printf("%d:shader:\n%s\n", shader, log);
}

void showprogramlog(GLint shader)
{
   // Prints the information log for a program object
   char log[1024];
   glGetProgramInfoLog(shader,sizeof log,nullptr,log);
   printf("%d:program:\n%s\n", shader, log);


} 
#endif
