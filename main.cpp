#include <stdio.h>
#include <cassert>
#include <string>
#include <signal.h>
#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>

#include <SDL2/SDL.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "stb_image.h"
#include <linux/input.h>
#include <AL/al.h>
#include <AL/alext.h>
#include <sndfile.h>

#include "shader_util.h"
#include "window.h"
#include "grid.h"

using namespace glm;


// Globals
static int mouse_fd = -1;
static CUBE_STATE_T _state, *state=&_state;
static SDL_GameController* controller = nullptr;
static volatile int keepRunning = 1;
static int digiMoveH = 0;
static int digiMoveV = 0;
static bool btnAPressed = false;
static bool btnBPressed = false;
static double mouseX = 0; 
static double mouseY = 0;
static ALuint source;
static Grid* grid;

// End Globals

void intHandler(int dummy) {
	keepRunning = 0;
}


static void get_mouse_alt(CUBE_STATE_T* state, int *outx, int *outy)  {
	struct input_event ev;
	
	if (mouse_fd < 0) 
	{
		mouse_fd = open("/dev/input/event0", O_RDONLY | O_NONBLOCK);
	}
	if (mouse_fd >= 0)  
	{

		// Read 5 times in a row, does this make the mouse less laggy?
		// It does :) 
		for (int i = 0; i < 5; i++)  {
			int bytes = read(mouse_fd, &ev, sizeof(ev));
			//SDL_Log("read bytes: %d", bytes);
			//SDL_Log("type: %d", ev.type);
			if (ev.type == EV_REL) 
			{
				if (ev.code == REL_X) 
				{
					// Assuming a projection with the actual screen dimensions, 
					// so we no longer need to divide by the screen width & height:
					//mouseX += ((float)ev.value/(float)state->screen_width);		
					mouseX += ev.value;		
					SDL_Log("mouseX: %f rawValue last: %d", mouseX, ev.value);
				}	
				if (mouseX < 0) {
					mouseX = 0;
				
				}
				if (mouseX > state->screen_width) {
					mouseX = state->screen_width;
				}
				if (ev.code == REL_Y) {
					mouseY -= ev.value;
				}
				if (mouseY < 0) {
					mouseY = 0;
				}
				if (mouseY > state->screen_height) {
					mouseY = state->screen_height;
				}
			}
			
		}
		
	}
}





static void init_shaders(CUBE_STATE_T *state)
{
   static const GLfloat vertex_data[] = {
        -.5,-.5,0,
        .5,-.5,0,
        0.5,0.5,.0,
				-0.5, 0.5, 0

   };
   const GLchar *vshader_source =
              "attribute vec3 vertex;"
              "attribute vec2 uvin;"
							"varying vec2 uv;"
							"uniform mat4 mworld;"
							"uniform mat4 mview;"
							"uniform mat4 mproj;"
              "void main(void) {"
              " vec4 pos = mproj * mview * mworld * vec4(vertex, 1);"
							" uv = uvin;"
              " gl_Position = pos;"
              "}";
      
   const GLchar *fshader_source =
"uniform sampler2D tex;"
"varying vec2 uv;"
"void main(void) {"
"  gl_FragColor = vec4(1, 1, 0, 1);"
"  gl_FragColor = texture2D(tex,uv);"
"}";


        state->vshader = glCreateShader(GL_VERTEX_SHADER);
				printf("vshader id: %u\n", state->vshader);
        glShaderSource(state->vshader, 1, &vshader_source, 0);
        glCompileShader(state->vshader);
				check();

        if (state->verbose)
            showlog(state->vshader);

        state->mshader = glCreateShader(GL_FRAGMENT_SHADER);
				printf("fragment shader: %u\n", state->mshader);
        glShaderSource(state->mshader, 1, &fshader_source, 0);
        glCompileShader(state->mshader);
        check();

        if (state->verbose)
            showlog(state->mshader);


        state->program2 = glCreateProgram();
        glAttachShader(state->program2, state->vshader);
        glAttachShader(state->program2, state->mshader);
        glLinkProgram(state->program2);
        check();

        if (state->verbose)
            showprogramlog(state->program2);
            
        state->attr_vertex = glGetAttribLocation(state->program2, "vertex");
        state->attr_uv = glGetAttribLocation(state->program2, "uvin");
        GLuint attr_normals = glGetAttribLocation(state->program2, "normals");
        state->unif_tex    = glGetUniformLocation(state->program2, "tex");       
				state->mat_world_idx = glGetUniformLocation(state->program2, "mworld");
				state->mat_view_idx = glGetUniformLocation(state->program2, "mview");
				state->mat_proj_idx = glGetUniformLocation(state->program2, "mproj");
				SDL_Log("mw loc: %d", state->mat_world_idx);
				SDL_Log("mv loc: %d", state->mat_view_idx);
				printf("attr_vertex %d\n", state->attr_vertex);
				printf("attr_uv %d\n", state->attr_uv);
				printf("attr_normals %d\n", attr_normals);
        
        glGenBuffers(1, &state->buf);
				printf("buf: %u\n", state->buf);
        check();

        // Prepare a texture image
        glGenTextures(1, &state->tex);
        check();
        glBindTexture(GL_TEXTURE_2D,state->tex);
        check();
				const char* cursorTex = "assets/curser32.png";
				const char* woodTex = "assets/wood64.png";
				int imageChannels, w, h;	
				uint8_t* imgBytes = stbi_load(cursorTex, &w, &h, &imageChannels, 4);
				//uint8_t* imgBytes = stbi_load("assets/space_shooter_sprite_tiles256.png", &w, &h, &imageChannels, 4);
				SDL_Log("loaded image w/h %d/%d", w, h);
				// TODO get rid of this assert!
				//assert(w == 64);

				// Test texture in memory
				GLubyte pixels[4*3] = {
					255, 0, 0, 
					0, 255, 0, 
					0, 0, 255, 
					255, 255, 0
				};
				//glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

				// Stuff works with our own in memory pixels but fails
				// with image from harddisk:
				// This is only for our hardcoded in-mem test image.
        //glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,2,2, 0,GL_RGB, GL_UNSIGNED_BYTE, pixels);

				// This is the "real" texture from the hard disk.
        glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,w,h, 0,GL_RGBA, GL_UNSIGNED_BYTE, imgBytes);

        check("after tex image");
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        check();

        // Prepare a framebuffer for rendering
        /*glGenFramebuffers(1,&state->tex_fb);
        check();
        glBindFramebuffer(GL_FRAMEBUFFER,state->tex_fb);
        check();
        glFramebufferTexture2D(GL_FRAMEBUFFER,GL_COLOR_ATTACHMENT0,GL_TEXTURE_2D,state->tex,0);
        check();
        glBindFramebuffer(GL_FRAMEBUFFER,0);
        check();
				*/

				// TODO probably get rid of this here - 
				// we are currently binding the active texture 
				// in the draw method and we are only using
				// one texture unit anyway. 
        glActiveTexture(GL_TEXTURE0);
        // Prepare viewport
        glViewport ( 0, 0, state->screen_width, state->screen_height );
        check();

				SDL_Log("sizeof vertex_data: %d", sizeof(vertex_data));
        
        // Upload vertex data to a buffer
        glBindBuffer(GL_ARRAY_BUFFER, state->buf);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_data),
                             vertex_data, GL_STATIC_DRAW);
        glVertexAttribPointer(state->attr_vertex, 3, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(state->attr_vertex);

				// UVs
				float uv_data[] = {
					0, 1, 
					1, 1, 
					1, 0, 
					0, 0
				};

				SDL_Log("attr_uv: %d", state->attr_uv);
				glGenBuffers(1, &state->uvBuffer);
				glBindBuffer(GL_ARRAY_BUFFER, state->uvBuffer);
				glBufferData(GL_ARRAY_BUFFER, sizeof(uv_data), uv_data, GL_STATIC_DRAW);
        glVertexAttribPointer(state->attr_uv, 2, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(state->attr_uv);

				// Index buffer
				GLubyte index_data[] = {
					0, 1, 2, 
					0, 2, 3
				};
				glGenBuffers(1, &state->indexBuffer);
				SDL_Log("indexbuffer: %d", state->indexBuffer);
				SDL_Log("indexbuffer size: %d", sizeof(index_data));
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, state->indexBuffer);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(index_data), index_data, GL_STATIC_DRAW);

        check();
}

static float posX = 0;
static float posY = 0;

void draw_triangles(CUBE_STATE_T* state) {

	// The update part - TODO move into separate function
	// This is for moving with the D-Pad on the xbox controller
	posX += 0.005 * (float)(digiMoveH);

	// This is temporary for moving our texture with the mouse:
	posX = mouseX;
	posY = mouseY;
	// end temp mouse cursor test.

	glBindBuffer(GL_ARRAY_BUFFER, state->buf);
	glBindBuffer(GL_ARRAY_BUFFER, state->uvBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, state->indexBuffer);
	check("after buffer bind");
	glUseProgram ( state->program2 );
	check("after use program");
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D,state->tex);
	
	//glUniform4f(state->unif_color, 0.5, 0.5, 0.8, 1.0);
	//glUniform2f(state->unif_scale, scale, scale);
	//glUniform2f(state->unif_offset, x, y);
	//glUniform2f(state->unif_centre, cx, cy);
	//glUniform1i(state->unif_tex, 0); // I don't really understand this part, perhaps it relates to active texture?


	glm::mat4 mt = glm::translate(glm::mat4(1), glm::vec3(posX, posY, 0));
	glm::mat4 ms = glm::scale(glm::mat4(1), vec3(16, 16, 1));
	glm::mat4 mv = glm::lookAt(vec3{0, 0, 1}, vec3{0, 0, -2}, vec3{0, 1, 0});
	glm::mat4 mp = glm::ortho<float>(0, state->screen_width, 0, state->screen_height, 0.01f, 100);
	glm::mat4 mw = mt * ms;
	glUniformMatrix4fv(state->mat_world_idx, 1, false, glm::value_ptr(mw));
	glUniformMatrix4fv(state->mat_view_idx, 1, false, glm::value_ptr(mv));
	glUniformMatrix4fv(state->mat_proj_idx, 1, false, glm::value_ptr(mp));
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, (GLvoid*) 0);
	check("after drawElements");

}


void initController() {
	if (SDL_Init(SDL_INIT_GAMECONTROLLER) < 0) 
	{
		SDL_Log("failure initializing SDL");
		exit(1);
	}
	SDL_Log("num joysticks: %d", SDL_NumJoysticks());
	if (SDL_NumJoysticks() == 0) {
		SDL_Log("Error, no gamecontroller found!");
		exit(1);
	}

	if (SDL_IsGameController(0)) 
	{
		SDL_Log("it is a game controller");
		controller = SDL_GameControllerOpen(0);
	}
	if (controller) {
		SDL_Log("opened the game controller(0)");
	}

	
	

}

void checkInput() {

  // GameController input
	// Reset the movement inputs
	digiMoveH = 0;
	digiMoveV = 0;

	SDL_Event event;	
	while (SDL_PollEvent(&event))   {
	
		if (event.type == SDL_CONTROLLERBUTTONDOWN) {
			if (event.cbutton.button == SDL_CONTROLLER_BUTTON_DPAD_LEFT) {
				digiMoveH = -1;
			}
			if (event.cbutton.button == SDL_CONTROLLER_BUTTON_DPAD_RIGHT) {
				digiMoveH = 1;
			}
			if (event.cbutton.button == SDL_CONTROLLER_BUTTON_START) {
			}

			if (event.cbutton.button == SDL_CONTROLLER_BUTTON_INVALID) {
			}
			if (event.cbutton.button == SDL_CONTROLLER_BUTTON_BACK) {
			}
			if (event.cbutton.button == SDL_CONTROLLER_BUTTON_GUIDE) {
				keepRunning = 0;
			}
		}
		
	}

	// Mouse input
	// Read raw input from the dev/input/event0 file:
	int mx, my;
	get_mouse_alt(state, &mx, &my);


}

void initSound(int argc, char** args)  {
	ALCdevice *device = nullptr;
	ALCcontext* ctx = nullptr;
	
	device = alcOpenDevice(NULL);
	if (!device) {
		fprintf(stderr, "Could not open device!\n");
		exit(1);
	}

	ctx = alcCreateContext(device, NULL);
	if (ctx == nullptr || alcMakeContextCurrent(ctx) == ALC_FALSE) {
		if (ctx) {
			alcDestroyContext(ctx);
		}
		alcCloseDevice(device);
		fprintf(stderr, "Could not create OpenAL context\n");
		exit(1);
	}

	ALenum err, format;
	ALuint buffer;
	SNDFILE* sndFile;
	SF_INFO sfinfo;
	short *membuf = nullptr;
	sf_count_t num_frames;
	ALsizei num_bytes;
	sndFile = sf_open("assets/thinline_16.wav", SFM_READ, &sfinfo);
	if (!sndFile) {
		SDL_Log("error reading soundfile");
		exit(1);
	}

	if (sfinfo.frames <  1 || sfinfo.frames > (sf_count_t)(INT_MAX/sizeof(short))/sfinfo.channels)
	{
		SDL_Log("error with sound frames");
		exit(1);
	}
	
	format = AL_NONE;
	if (sfinfo.channels == 1) {
		format = AL_FORMAT_MONO16;
	}
	else if (sfinfo.channels = 2) {
		format = AL_FORMAT_STEREO16;
	
	}
	else if  (sfinfo.channels == 3) {
		SDL_Log("3 channels");
		exit(1);
	}
	else if (sfinfo.channels = 4) {
		SDL_Log("4 channels");
		exit(1);
	}

	if (!format) {
		SDL_Log("no format");
		exit(1);
	}


	membuf = (short*) malloc((size_t)(sfinfo.frames * sfinfo.channels) * sizeof(short));
	num_frames = sf_readf_short(sndFile, membuf, sfinfo.frames);
	if (num_frames < 1) {
		
		free(membuf);
		sf_close(sndFile);

	}
	num_bytes = (ALsizei)(num_frames * sfinfo.channels) * (ALsizei)sizeof(short);

	buffer = 0;
	alGenBuffers(1, &buffer);
	alBufferData(buffer, format, membuf, num_bytes, sfinfo.samplerate);
	
	free(membuf);
	sf_close(sndFile);
	
	err = alGetError();
	if (err != AL_NO_ERROR) {
		SDL_Log("open al error");
		exit(1);

	}
	ALfloat offset;
	alGenSources(1, &source);
	alSourcei(source, AL_BUFFER, (ALint)buffer);
	assert(alGetError()==AL_NO_ERROR && "Failed to setup sound source");

	
	ALenum soundState;
	alSourcePlay(source);
	/*do {
	  sleep(5);
		alGetSourcei(source, AL_SOURCE_STATE, &soundState);
	
	} while (alGetError() == AL_NO_ERROR && soundState == AL_PLAYING);
	*/
	SDL_Log("sound finished playing");	



}



int main(int argc, char** args) {

	
	signal(SIGINT, intHandler);
  memset( state, 0, sizeof( *state ) );
	printf("starting..\n");

	Window win(state);
	init_shaders(state);
	grid = new Grid();
	initController();
	initSound(argc,args);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
  glCullFace(GL_BACK);
	glFrontFace(GL_CCW);


	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);

  // Set background color and clear buffers
  glClearColor(0.5f, 0.3f, 0.2f, 1.0f);
  check("check0"); 

  keepRunning = 1;
  while (keepRunning)
  {
      int x, y, b;
			glBindFramebuffer(GL_FRAMEBUFFER,0);
      glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

			checkInput();
      check("check1"); 
			draw_triangles(state);
		  //grid->render(0, 0, 0, 0);
      check("check2"); 

      glFlush();
			glFinish();
        
      eglSwapBuffers(state->display, state->surface);
   }

	close(mouse_fd);
	ALCcontext* ctx = alcGetCurrentContext();
	if (ctx != nullptr) {
		ALCdevice* device = alcGetContextsDevice(ctx);
		alcMakeContextCurrent(NULL);
		alcDestroyContext(ctx);
		alcCloseDevice(device);
	}
	
  return 0;

}
