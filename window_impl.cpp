// This file only serves the purpose to 
// trigger the compilation of our header-only files. 

#define WINDOW_IMPL
#include "window.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#define GRID_IMPL
#include "grid.h"

