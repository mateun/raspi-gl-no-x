#pragma once

struct CUBE_STATE_T
{
   uint32_t screen_width;
   uint32_t screen_height;
// OpenGL|ES objects
   EGLDisplay display;
   EGLSurface surface;
   EGLContext context;

   GLuint verbose;
   GLuint vshader;
   GLuint fshader;
   GLuint mshader;
   GLuint program;
   GLuint program2;
   GLuint tex_fb;
   GLuint tex;
   GLuint buf;
	 GLuint indexBuffer;
	 GLuint uvBuffer;

	 
// julia attribs
   GLuint unif_color, attr_vertex, unif_scale, unif_offset, unif_tex, unif_centre; 
// mandelbrot attribs
   GLuint attr_vertex2, unif_scale2, unif_offset2, unif_centre2;
	 GLuint attr_uv;
	
	 GLuint mat_world_idx;
	 GLuint mat_view_idx;
	 GLuint mat_proj_idx;

};
